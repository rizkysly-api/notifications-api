<?php

class api_apns_dev extends api_apns
{
    protected static $service = 'apns_dev';
    protected static $url = 'https://api.development.push.apple.com:443';

}
