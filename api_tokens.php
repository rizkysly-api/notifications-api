<?php

class api_tokens
{

    // Set or update the device token in the database
    public static function set(): void
    {
        // Make sure we only exept services we can process (apns at the moment, gcm may come later)
        if (!isset($_POST['service']) || !in_array($_POST['service'], array('apns', 'apns_dev'))) {
            api_security::generateError('400 Bad Request (service)');
        }

        // A push notification token is 64 charakters long (apns) or longer (gcm)
        // We are doing no exact tests since the format can change in the future without notice
        if (!isset($_POST['token']) || strlen(trim($_POST['token'])) < 64) {
            api_security::generateError('400 Bad Request (token)');
        }

        // We can check if a user has authorized push notifications
        if (!isset($_POST['status'])) {
            api_security::generateError('400 Bad Request (status)');
        }

        // Add the device/token or update if the device already exists
        $stmt = api_database::prepare('INSERT INTO notify_tokens (app_uuid, device_uuid, service, token, status) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE service = ?, token = ?, status = ?, contactdate = NOW()');
        $stmt->bind_param('ssssssss', $_SERVER['HTTP_X_APPLICATION_KEY'], $_POST['device'], $_POST['service'], $_POST['token'], $_POST['status'], $_POST['service'], $_POST['token'], $_POST['status']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }

    // Set or update the device token in the database
    public static function badge(): void
    {
        // Make sure we only exept services we can process (apns at the moment, gcm may come later)
        if (!isset($_POST['service']) || !in_array($_POST['service'], array('apns', 'apns_dev'))) {
            api_security::generateError('400 Bad Request (service)');
        }

        if (!isset($_POST['badge']) || $_POST['badge'] < 0) {
            api_security::generateError('400 Bad Request (badge)');
        }

        // Update the device/badge
        $stmt = api_database::prepare('UPDATE notify_tokens SET badge = ? WHERE app_uuid = ? AND device_uuid = ? AND service = ?');
        $stmt->bind_param('isss', $_POST['badge'], $_SERVER['HTTP_X_APPLICATION_KEY'], $_POST['device'], $_POST['service']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}
