<?php

class api_queue
{

    public static function set() : void
    {
        // Create payload
        $payload = api_apns::payload();
        if (strlen($payload) > 4096) {
            api_security::generateError('400 Bad Request (payload)');
        }

        // Queue the message with the correct token information (Or ignore if not found)
        $stmt = api_database::prepare('INSERT INTO notify_queue (app_uuid, service, token, payload) 
                                        SELECT app_uuid, service, token, ? FROM notify_tokens WHERE device_uuid = ?');
        $stmt->bind_param('ss', $payload, $_POST['device']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}