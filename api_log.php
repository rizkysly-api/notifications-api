<?php

class api_log
{
    // See if we need to alert somebody 
    public static function check(): void
    {
        $errors = [];
        $stmt = api_database::prepare('SELECT code, error, COUNT(adddate) FROM notify_log WHERE adddate > NOW() - INTERVAL 1 HOUR GROUP BY code, error ORDER BY code');
        $stmt->execute();
        $stmt->bind_result($code, $error, $adddate);
        while ($stmt->fetch()) {
            $errors[] = $adddate . ' ' . $code . ' ' . $error;
        }
        $stmt->close();

        // This wil print to the cron tab log or if the server is nice, to the local mailbox.
        echo implode(PHP_EOL, $errors);
    }
}
