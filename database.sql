-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 30 dec 2018 om 13:05
-- Serverversie: 5.7.24-0ubuntu0.18.04.1
-- PHP-versie: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_rizkysly`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notify_log`
--

CREATE TABLE `notify_log` (
  `id` int(11) NOT NULL,
  `app_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `service` enum('apns','apns_test') NOT NULL,
  `token` varchar(128) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `payload` varchar(4096) DEFAULT NULL,
  `adddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` int(11) DEFAULT NULL,
  `error` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notify_planned`
--

CREATE TABLE `notify_planned` (
  `id` int(11) NOT NULL,
  `requesting_app_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `device_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `identifier` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payload` varchar(4096) DEFAULT NULL,
  `adddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processing_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notify_queue`
--

CREATE TABLE `notify_queue` (
  `id` int(11) NOT NULL,
  `app_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `service` enum('apns','apns_test') DEFAULT NULL,
  `token` varchar(128) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `payload` varchar(4096) DEFAULT NULL,
  `adddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processing_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `notify_tokens`
--

CREATE TABLE `notify_tokens` (
  `app_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `device_uuid` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `service` enum('apns','apns_dev') DEFAULT NULL,
  `token` varchar(1024) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `status` enum('notDetermined','denied','authorized','provisional') DEFAULT 'notDetermined',
  `contactdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `notify_log`
--
ALTER TABLE `notify_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `notify_planned`
--
ALTER TABLE `notify_planned`
  ADD PRIMARY KEY (`id`),
  ADD KEY `senddate` (`time`),
  ADD KEY `app_uuid_2` (`requesting_app_uuid`,`identifier`);

--
-- Indexen voor tabel `notify_queue`
--
ALTER TABLE `notify_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `notify_tokens`
--
ALTER TABLE `notify_tokens`
  ADD PRIMARY KEY (`device_uuid`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `notify_log`
--
ALTER TABLE `notify_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `notify_planned`
--
ALTER TABLE `notify_planned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `notify_queue`
--
ALTER TABLE `notify_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
