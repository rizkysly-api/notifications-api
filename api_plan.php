<?php



// Apps might want to plan some notifications for the user. When using cloud system, local notifications are not ideal.
class api_plan
{

    // Everytime we plan notifications, clear the old ones
    public static function clear(): void
    {
        // Do we have a identifier? Is it an uuidv4?
        if (!isset($_POST['identifier']) || !api_uuidv4::check($_POST['identifier'])) {
            api_security::generateError('400 Bad Request (identifier)');
        }

        // Delete from database, based on app and identifier uuid
        $stmt = api_database::prepare('DELETE FROM notify_planned WHERE requesting_app_uuid = ? AND identifier = ?');
        $stmt->bind_param('ss', $_SERVER['HTTP_X_APPLICATION_KEY'], $_POST['identifier']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }

    // Plan a notification
    public static function set(): void
    {
        // Do we have a identifier? Is it an uuidv4?
        if (!isset($_POST['identifier']) || !api_uuidv4::check($_POST['identifier'])) {
            api_security::generateError('400 Bad Request (identifier)');
        }

        // Do we have a time? Is it an datetime?
        if (!isset($_POST['time']) || $_POST['time'] - time() < 0) {
            api_security::generateError('400 Bad Request (time)');
        }

        // Create payload
        $payload = api_apns::payload();
        if (strlen($payload) > 4096) {
            api_security::generateError('400 Bad Request (payload)');
        }

        // Store planned payload
        $date = date('Y-m-d H:i:s', $_POST['time']);
        $stmt = api_database::prepare('INSERT INTO notify_planned (requesting_app_uuid, device_uuid, identifier, time, payload) VALUES (?, ?, ?, ?, ?)');
        $stmt->bind_param('sssss', $_SERVER['HTTP_X_APPLICATION_KEY'], $_POST['device'], $_POST['identifier'], $date, $payload);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }

    // Lets see if we have any planned notifications we want to send along
    public static function queue()
    {
        // Makeing a unique uuid makes sure everything wil operate nicely, even if two processes are running at the same time
        $processing_uuid = api_uuidv4::generate();

        // Set the notifications as queueing so we know for sure we delete the correct ones 
        $stmt = api_database::prepare('UPDATE notify_planned SET processing_uuid = ? WHERE processing_uuid IS NULL AND time <= NOW()');
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        api_database::check();
        $stmt->close();

        // Add planned notifications in the queue
        $stmt = api_database::prepare('INSERT INTO notify_queue (app_uuid, service, token, payload) 
                                        SELECT t.app_uuid, t.service, t.token, p.payload FROM notify_planned p LEFT JOIN notify_tokens t ON t.device_uuid = p.device_uuid WHERE p.processing_uuid = ? AND t.token IS NOT NULL');
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        api_database::check();
        $stmt->close();

        // Hopefully the queueing went wel, we are deleting now  
        $stmt = api_database::prepare('DELETE FROM notify_planned WHERE processing_uuid = ?');
        $stmt->bind_param('s', $processing_uuid);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}
