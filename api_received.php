<?php

class api_received
{

    public static function set(): void
    {
        // Do we have a identifier? Is it an uuidv4?
        if (!isset($_POST['identifier']) || !api_uuidv4::check($_POST['identifier'])) {
            api_security::generateError('400 Bad Request (identifier)');
        }

        // Mark the notification as received
        $stmt = api_database::prepare('UPDATE notify_received SET received = NOW() WHERE identifier = ?');
        $stmt->bind_param('s', $_POST['identifier']);
        $stmt->execute();
        api_database::check();
        $stmt->close();
    }
}
