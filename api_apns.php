<?php

class api_apns
{
    protected static $service = 'apns';
    protected static $url = 'https://api.push.apple.com:443';

    private static $ch = null;
    private static $start = 0;
    private static $processing_uuid = '';
    private static $app_uuid = '';
    private static $topic = '';

    // For testing purposes
    private static $debug = false;
    private static $verbose = null;

    // Send all apns push notifications
    public static function send(): void
    {
        self::$start = time(); // This script should run for 60 seconds

        // Crontab will run this script every minute, so lets keep php busy for that time
        while (time() - self::$start < 60) {

            // Create unique id so we know what entries we are handling now.
            self::$processing_uuid = api_uuidv4::generate();

            // Send a batch
            self::batch();

            // Make sure the sending uuid is reset on any remaining notifications (connection, destination and this can be broken...)
            $stmt = api_database::prepare('UPDATE notify_queue SET processing_uuid = NULL WHERE service = ? AND processing_uuid = ?');
            $stmt->bind_param('ss', static::$service, self::$processing_uuid);
            $stmt->execute();
            api_database::check();
            $stmt->close();

            // Sleep for a second to keep the server serene
            sleep(1);
        }

        // Close the connection nicely when ending this script
        if (self::$ch) {
            curl_close(self::$ch);
            self::$ch = null;
        }

        // For testing purposes
        if (self::$debug && self::$verbose) {
            rewind(self::$verbose);
            echo stream_get_contents(self::$verbose);
        }
    }

    // We are sending everything in batches, so we can control the flow
    private static function batch()
    {
        //Can we sent some stuff?
        $count = 0;
        $stmt = api_database::prepare('SELECT COUNT(id) FROM notify_queue WHERE service = ? AND processing_uuid IS NULL');
        $stmt->bind_param('s', static::$service);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();

        if ($count == 0) {
            return;
        }

        // Mark notifications as sending, do we need a process ID? Lets try a limited numer at a time
        $stmt = api_database::prepare('UPDATE notify_queue SET processing_uuid = ? WHERE service = ? AND processing_uuid IS NULL ORDER BY adddate LIMIT ' . NOTIFICATION_ENTRY_LIMIT);
        $stmt->bind_param('ss', self::$processing_uuid, static::$service);
        $stmt->execute();
        api_database::check();
        $stmt->close();

        // Get all notifications we want to send
        $notifications = [];
        $id = $app_uuid = $token = $payload = $status = null;
        $stmt = api_database::prepare('SELECT DISTINCT q.id, q.app_uuid, q.token, q.payload, t.badge, t.status 
                                        FROM notify_queue q LEFT JOIN notify_tokens t ON t.token = q.token AND t.service = ?
                                        WHERE q.processing_uuid = ? ORDER BY q.app_uuid');
        $stmt->bind_param('ss', static::$service, self::$processing_uuid);
        $stmt->execute();
        $stmt->bind_result($id, $app_uuid, $token, $payload, $badge, $status);
        while ($stmt->fetch()) {
            $notifications[] = [
                'id' => $id,
                'app_uuid' => $app_uuid,
                'token' => $token,
                'badge' => $badge,
                'payload' => $payload,
                'status' => $status
            ];
        }
        $stmt->close();

        // Loop throu the notifications
        foreach ($notifications as $notification) {

            // When app_uuid changes, we need to change some configurations
            $info = null;
            if (self::$app_uuid != $notification['app_uuid']) {
                self::$app_uuid = $notification['app_uuid'];

                $info = APP_KEYS[self::$app_uuid][static::$service];

                if (self::$ch) {
                    curl_close(self::$ch);
                    self::$ch = null;
                }
            }

            // Create connection if it does not exist, and do we have something to send?
            if (!self::$ch) {
                self::$ch = curl_init();
                curl_setopt(self::$ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
                curl_setopt(self::$ch, CURLOPT_RETURNTRANSFER, true);
                //curl_setopt(self::$ch, CURLOPT_SSL_VERIFYPEER, 0);
                //curl_setopt(self::$ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt(self::$ch, CURLOPT_VERBOSE, self::$debug);
                curl_setopt(self::$ch, CURLOPT_SSLCERT, $info['pem']);
                curl_setopt(self::$ch, CURLOPT_SSLCERTPASSWD, '');

                self::$topic = $info['topic'];

                // Lets see what error handling we need, this is a bit to verbose
                if (self::$debug) {
                    self::$verbose = fopen('php://temp', 'w+');
                    curl_setopt(self::$ch, CURLOPT_STDERR, self::$verbose);
                }
            }

            if (!self::$ch) {
                return;
            }

            $payload = json_decode($notification['payload'], true);

            // Proceed when status is 'authorized','provisional' or if content-available is set
            if (in_array($notification['status'], ['authorized', 'provisional']) || isset($payload['aps']['content-available'])) {

                // Figure out what priority to give. The docs advise 10 only for user alerts/badges/sounds
                $priority = 5;
                if (isset($payload['aps']['alert']) || isset($payload['aps']['badge']) || isset($payload['aps']['sound'])) {
                    $payload['aps']['badge'] += $notification['badge'];
                    $priority = 10;
                }

                curl_setopt(self::$ch, CURLOPT_URL, static::$url . '/3/device/' . $notification['token']);
                curl_setopt(self::$ch, CURLOPT_POSTFIELDS, json_encode($payload));
                curl_setopt(self::$ch, CURLOPT_HTTPHEADER, array(
                    'apns-topic: ' . self::$topic,
                    'apns-priority: ' . $priority
                ));

                // Send a notification
                $result = curl_exec(self::$ch);
                $httpcode = curl_getinfo(self::$ch, CURLINFO_HTTP_CODE);

                echo json_encode($payload) . ' - ' . $httpcode . ' - ' . json_encode($result) . PHP_EOL;

                // Did we lose the connection?
                if (!self::$ch) {
                    return;
                }

                // The apns server is broken, lets abort so we don't make things worst
                if ($httpcode >= 500) {
                    return;
                }

                // Log any errors we receive, ignoring invalid tokens
                $error = json_decode($result);
                if (json_last_error() == JSON_ERROR_NONE && isset($error->reason) && $httpcode != 410 && $error->reason != 'BadDeviceToken') {
                    $stmt = api_database::prepare('INSERT INTO notify_log (app_uuid, service, token, payload, code, error) VALUES (?, ?, ?, ?, ?, ?)');
                    $stmt->bind_param('ssssis', self::$app_uuid, $notification['token'], static::$service, $notification['payload'], $httpcode, $error->reason);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();
                }

                // The certificate is broken we have to alert an administrator
                if ($httpcode == 403) {
                    continue;
                }

                // The device token is no longer valid, lets delete it from the database
                if ($httpcode == 410 || (isset($error->reason) && $error->reason == 'BadDeviceToken')) {
                    $stmt = api_database::prepare('DELETE FROM notify_tokens WHERE service = ? AND token = ?');
                    $stmt->bind_param('ss', static::$service, $notification['token']);
                    $stmt->execute();
                    api_database::check();
                    $stmt->close();

                    continue;
                }

                // Lets log the message id so we can track success rates, when apps receive the notification, it should confirm it to the server
                $stmt = api_database::prepare('INSERT INTO notify_received (service, identifier) VALUES (?, ?)');
                $stmt->bind_param('ss', static::$service, $payload['identifier']);
                $stmt->execute();
                $stmt->close();

                // Ubdate the badge value in the token table
                $stmt = api_database::prepare('UPDATE notify_tokens SET badge = ? WHERE service = ? AND token = ?');
                $stmt->bind_param('iss', $payload['aps']['badge'], static::$service, $notification['token']);
                $stmt->execute();
                api_database::check();
                $stmt->close();
            }

            // Delete the notification from the database
            $stmt = api_database::prepare('DELETE FROM notify_queue WHERE id = ?');
            $stmt->bind_param('i', $notification['id']);
            $stmt->execute();
            api_database::check();
            $stmt->close();
        }
    }

    // Lets create the payload we can send to apns
    public static function payload(): string
    {
        $object = ['identifier' => api_uuidv4::generate()];

        if (isset($_POST['title'])) {
            $object['aps']['alert']['title'] = $_POST['title'];
        }
        if (isset($_POST['body'])) {
            $object['aps']['alert']['body'] = $_POST['body'];
        }
        if (isset($_POST['badge'])) {
            $object['aps']['badge'] = (int) $_POST['badge'];
        }
        if (isset($_POST['sound'])) {
            $object['aps']['sound'] = $_POST['sound'];
        }
        if (isset($_POST['category'])) {
            $object['aps']['category'] = $_POST['category'];
        }
        $object['aps']['content-available'] = 1;
        if (isset($_POST['content-available'])) {
            $object['aps']['content-available'] = 1;

            if (isset($_POST['data'])) {
                $data = json_decode($_POST['data'], true);

                if (json_last_error() == JSON_ERROR_NONE && !isset($data['aps'])) {
                    $object = array_merge($object, $data);
                }
            }
        }

        return json_encode($object, JSON_FORCE_OBJECT);
    }
}
