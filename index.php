<?php

// Quick and dirty, and it works...
include(__DIR__ . '/configuration.php');
include(__DIR__ . '/api-generics/api_database.php');
include(__DIR__ . '/api-generics/api_request.php');
include(__DIR__ . '/api-generics/api_security.php');
include(__DIR__ . '/api-generics/api_uuidv4.php');
include(__DIR__ . '/api_apns.php');
include(__DIR__ . '/api_apns_dev.php');
include(__DIR__ . '/api_log.php');
include(__DIR__ . '/api_plan.php');
include(__DIR__ . '/api_queue.php');
include(__DIR__ . '/api_tokens.php');
include(__DIR__ . '/api_received.php');

// Handle cli requests (cron jobs)
if (php_sapi_name() == 'cli') {
    if (!isset($argv[1])) {
        exit(0);
    }

    switch ($argv[1]) {
        case 'queue':
            api_plan::queue();
            break;
        case 'send_apns':
            api_apns::send();
            break;
        case 'send_apns_dev':
            api_apns_dev::send();
            break;
        case 'log':
            api_log::check();
            break;
    }

    exit(0);
}

// Checks if the request is valid, signed and so on
api_security::checkRequest();

// Routing; Just make sure all traffic is rewrited to this file
if (isset($_SERVER['REQUEST_URI'])) {
    $action = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_BASENAME);
    switch ($action) {
        case 'token':
            api_tokens::set();
            break;
        case 'badge':
            api_tokens::badge();
            break;
        case 'send':
            api_queue::set();
            break;
        case 'received':
            api_received::set();
            break;
        case 'set':
            api_plan::set();
            break;
        case 'clear':
            api_plan::clear();
            break;
        default:
            api_security::generateError('404 File not found');
    }
}

// If everything is fine, we do not return any data, just a 200 status code, exept when it is expected
