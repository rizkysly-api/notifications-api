# Notifications API 
Plan, set and send push notifications to registered devices.

## Installation
`git clone https://gitlab.com/rizkysly-api/notifications-api.git`  
`git submodule update --init --recursive`

Import the `database.sql` file into your mysql database.

Rename `configuration_example.php` to configuration.php and change the files constants.  

Setup an url-rewrite rule to send all traffic to `index.php`.  

Add the following cron tasks by running `crontab -e`
```
* * * * * php -f /location/to/index.php queue  
* * * * * php -f /location/to/index.php send_apns  
* * * * * php -f /location/to/index.php send_apns_dev  
0 * * * * php -f /location/to/index.php log  
```

## Requests
Please see https://gitlab.com/rizkysly-api/api-generics.git for request specifications. The post values here are on top of the once described in api-generics.

**/token**  
Register the apps notifications token.  
>>>
device=Device key
service=apns or apns_dev  
token=token_string  
status=notDetermined, denied, authorized or provisional  
>>>

**/send**  
Directly sent notifications. The following post keys are optional. But in theory the title or content-available is required.  
>>>
device=The device key the notification is intended for
title=Title of notification  
body=Body of notification  
badge=Badge number of notification  
category=Category of notification  
content-available=Set content available to 1  
data=json string of extra items to include in the notification  
>>>

**/set**  
Plan notifications in advanced. When the time comes, they will be sent. See the `/send` request for payload keys.
>>>
device=The device key the notification is intended for  
identifier=Identifies your notification or set of notifications.  
time=yyyy-mm-dd hh:mm:ss
>>>

**/clear**  
Removes previously planned notifications from the database. Choose your identifiers carefully, it will delete all planned notifications with this identifier.
>>>
identifier=Identifies your notification or set of notifications.  
>>>