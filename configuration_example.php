<?php

// List all applications keys (`APP_KEY` and `APP_SECRET` should be uuidv4's)
// - App secrets, to set/check the request signatures
// - Push notification server specifics
define('APP_KEYS', [
    'APP_KEY' => [
        'secret' => 'APP_SECRET',
        'apns' => [
            'topic' => '',
            'pem' => 'pro.pem',
            'pem_password' => ''
        ],
        'apns_dev' => [
            'topic' => '',
            'pem' => 'dev.pem',
            'pem_password' => ''
        ]
    ]
]);

// Talking to a database require database credentials
define('DB_HOST', 'localhost');
define('DB_DATABASE', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');

// Some throtteling stuff
define('FAILED_ATTEMPTS_NUMBER', 10); // How many failed attempts can an ip make
define('FAILED_ATTEMPTS_MINUTES', 10); // In how much time
define('TIMESTAMP_AGE_SECONDS', 300); // What can the timestamp difference be

// Lets limit the number of notification we process at a time, we can increment it if this number is to low
define('NOTIFICATION_ENTRY_LIMIT', 100);
